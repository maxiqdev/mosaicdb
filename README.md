# DB Package For Mosaic Decisions

## Prerequisites

* Python
* PIP

## Installing database

```
git clone git@bitbucket.org:maxiqdev/mosaicdb.git
pip install mosaicdb
```

## Default users

```
Username: sam@lntinfotech.com
Password: Newuser@1234
```

## Commands available

* install-mosaicdb
* install-mosaicdb-schema
* install-mosaicdb-data

## Usage

#### install-mosaicdb

```
(env) akhil:mosaicdb akhilputhiry$ install-mosaicdb --help
Usage: install-mosaicdb [OPTIONS]

  Install schema and data for mosaic decisions

Options:
  --host TEXT      Hostname of the MySql server. Default value is `localhost`
  --port INTEGER   Port on which MySql server is running. Default value is
                   `3306`
  --database TEXT  Database name. Default value is `maxiq`
  --username TEXT  Username of the MySql user. Default value is `root`
  --password TEXT  Password of the MySql user. Default value is `root`
  --help           Show this message and exit.
```

#### install-mosaicdb-schema

```
(env) akhil:mosaicdb akhilputhiry$ install-mosaicdb-schema --help
Usage: install-mosaicdb-schema [OPTIONS]

  Install schema for mosaic decisions

Options:
  --host TEXT      Hostname of the MySql server. Default value is `localhost`
  --port INTEGER   Port on which MySql server is running. Default value is
                   `3306`
  --database TEXT  Database name. Default value is `maxiq`
  --username TEXT  Username of the MySql user. Default value is `root`
  --password TEXT  Password of the MySql user. Default value is `root`
  --help           Show this message and exit.
```

#### install-mosaicdb-data

```
(env) akhil:mosaicdb akhilputhiry$ install-mosaicdb-data --help
Usage: install-mosaicdb-data [OPTIONS]

  Install data for mosaic decisions

Options:
  --host TEXT      Hostname of the MySql server. Default value is `localhost`
  --port INTEGER   Port on which MySql server is running. Default value is
                   `3306`
  --database TEXT  Database name. Default value is `maxiq`
  --username TEXT  Username of the MySql user. Default value is `root`
  --password TEXT  Password of the MySql user. Default value is `root`
  --help           Show this message and exit.
```

## Sample output

```
(env) akhil:mosaicdb akhilputhiry$ install-mosaicdb --database=akhil
Installing schema...
--- STDOUT ---
b''
--- STDERR ---
b'mysql: [Warning] Using a password on the command line interface can be insecure.\n'
Installing data...
--- STDOUT ---
b''
--- STDERR ---
b'mysql: [Warning] Using a password on the command line interface can be insecure.\n'
```
