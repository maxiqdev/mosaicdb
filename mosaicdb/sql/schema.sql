-- MySQL dump 10.13  Distrib 5.6.37, for Linux (x86_64)
--
-- Host: localhost    Database: maxiq
-- ------------------------------------------------------
-- Server version	5.6.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actions_master`
--

DROP TABLE IF EXISTS `actions_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions_master` (
  `actionsMasterId` int(11) NOT NULL AUTO_INCREMENT,
  `appId` int(11) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `subsetOf` varchar(40) DEFAULT NULL,
  `actionCode` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`actionsMasterId`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ownerProjectId` int(20) DEFAULT NULL,
  `objectType` varchar(45) DEFAULT NULL,
  `objectId` varchar(45) DEFAULT NULL,
  `userId` varchar(45) DEFAULT NULL,
  `timeDate` varchar(45) DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `objectName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appViewInstance`
--

DROP TABLE IF EXISTS `appViewInstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appViewInstance` (
  `Id` varchar(100) DEFAULT NULL,
  `appId` varchar(200) DEFAULT NULL,
  `inputParametersValues` blob,
  `jobId` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_global_param`
--

DROP TABLE IF EXISTS `app_global_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_global_param` (
  `id` int(20) DEFAULT NULL,
  `type` varchar(40) DEFAULT NULL,
  `componantId` varchar(20) DEFAULT NULL,
  `appId` varchar(20) DEFAULT NULL,
  `globalParamValue` varchar(40) DEFAULT NULL,
  `globalParamName` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_graph_nodes`
--

DROP TABLE IF EXISTS `app_graph_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_graph_nodes` (
  `id` varchar(40) DEFAULT NULL,
  `appName` varchar(100) DEFAULT NULL,
  `appId` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `nodeType` varchar(40) DEFAULT NULL,
  `custCompName` varchar(500) DEFAULT NULL,
  `nodeDescription` longblob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_output_files`
--

DROP TABLE IF EXISTS `app_output_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_output_files` (
  `id` varchar(50) DEFAULT NULL,
  `appId` varchar(50) DEFAULT NULL,
  `appInstanceId` varchar(50) DEFAULT NULL,
  `nodeId` varchar(100) DEFAULT NULL,
  `path` varchar(500) DEFAULT NULL,
  `appName` varchar(500) DEFAULT NULL,
  `label` varchar(500) DEFAULT NULL,
  `lastRun` varchar(100) DEFAULT NULL,
  `lastrefreshBy` varchar(100) DEFAULT NULL,
  `nodeName` varchar(200) DEFAULT NULL,
  `savedForExploration` varchar(100) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `keyName` varchar(50) DEFAULT NULL,
  `compressionType` varchar(20) DEFAULT NULL,
  `fileType` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_user`
--

DROP TABLE IF EXISTS `application_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_user` (
  `unqUserId` int(20) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(40) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `userUniqueId` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `password` blob,
  `userBrowsers` varchar(400) DEFAULT NULL,
  `emailId` varchar(4000) DEFAULT NULL,
  `userCreatedDate` varchar(40) DEFAULT NULL,
  `userPlatform` varchar(400) DEFAULT NULL,
  `userId` varchar(40) DEFAULT NULL,
  `userips` varchar(400) DEFAULT NULL,
  `firstName` varchar(40) DEFAULT NULL,
  `passwordexpirydays` varchar(40) DEFAULT NULL,
  `userGroup` varchar(40) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `groupName` varchar(50) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `createdTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` varchar(40) DEFAULT NULL,
  `updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `passwordRefreshedDate` varchar(50) DEFAULT NULL,
  `rangerUserName` varchar(255) DEFAULT NULL,
  `userRangerId` varchar(225) DEFAULT '0',
  `lastUsedPersonaId` varchar(255) DEFAULT NULL,
  `lockInd` varchar(10) NOT NULL DEFAULT 'UNLOCK',
  `lastFailureAttemptTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `loginFailureCount` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`unqUserId`),
  KEY `idx_unqUserId` (`unqUserId`)
) ENGINE=MyISAM AUTO_INCREMENT=326 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_user_history`
--

DROP TABLE IF EXISTS `application_user_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_user_history` (
  `appUserHist_Id` int(20) NOT NULL AUTO_INCREMENT,
  `his_unqUserId` int(10) DEFAULT NULL,
  `his_lastName` varchar(40) DEFAULT NULL,
  `his_status` varchar(40) DEFAULT NULL,
  `his_userUniqueId` varchar(40) DEFAULT NULL,
  `his_class` varchar(40) DEFAULT NULL,
  `his_password` blob,
  `his_userBrowsers` varchar(400) DEFAULT NULL,
  `his_emailId` varchar(40) DEFAULT NULL,
  `his_userCreatedDate` varchar(40) DEFAULT NULL,
  `his_userPlatform` varchar(400) DEFAULT NULL,
  `his_userId` varchar(40) DEFAULT NULL,
  `his_userips` varchar(400) DEFAULT NULL,
  `his_firstName` varchar(40) DEFAULT NULL,
  `his_passwordexpirydays` varchar(40) DEFAULT NULL,
  `his_userGroup` varchar(40) DEFAULT NULL,
  `his_groupId` int(10) DEFAULT NULL,
  `his_groupName` varchar(40) DEFAULT NULL,
  `his_createdBy` varchar(40) DEFAULT NULL,
  `his_createdTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `his_updatedBy` varchar(40) DEFAULT NULL,
  `his_updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `groupIdList` varchar(1000) DEFAULT NULL,
  `groupNameList` varchar(1000) DEFAULT NULL,
  `subGroupIdList` varchar(1000) DEFAULT NULL,
  `subGroupNameList` varchar(1000) DEFAULT NULL,
  `roleIdList` varchar(1000) DEFAULT NULL,
  `roleNameList` varchar(1000) DEFAULT NULL,
  `action` varchar(30) DEFAULT NULL,
  `insertedBy` varchar(40) DEFAULT NULL,
  `insertedTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lockInd` varchar(10) NOT NULL DEFAULT 'UNLOCK',
  `lastFailureAttemptTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `loginFailureCount` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`appUserHist_Id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blocked_url_action_mappings`
--

DROP TABLE IF EXISTS `blocked_url_action_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocked_url_action_mappings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `actionsMasterId` int(11) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blocked_url_persona_mappings`
--

DROP TABLE IF EXISTS `blocked_url_persona_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocked_url_persona_mappings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `personaId` int(11) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `boost`
--

DROP TABLE IF EXISTS `boost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orderLevel` varchar(40) DEFAULT NULL,
  `numberOfExecutors` varchar(10) DEFAULT NULL,
  `executorsMemory` varchar(10) DEFAULT NULL,
  `driverMemory` varchar(10) DEFAULT NULL,
  `numberOfCores` varchar(10) DEFAULT NULL,
  `isDefault` varchar(10) DEFAULT NULL,
  `insertedBy` varchar(40) DEFAULT NULL,
  `insertedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastModifiedBy` varchar(40) DEFAULT NULL,
  `lastModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `boost_mapping`
--

DROP TABLE IF EXISTS `boost_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boost_mapping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `objectType` varchar(50) DEFAULT NULL,
  `objectId` varchar(50) DEFAULT NULL,
  `boostId` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `boost_mapping_reduce`
--

DROP TABLE IF EXISTS `boost_mapping_reduce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boost_mapping_reduce` (
  `jobInstanceId` varchar(40) DEFAULT NULL,
  `boostId` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category_subcategory_master`
--

DROP TABLE IF EXISTS `category_subcategory_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_subcategory_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catOrSubCatName` varchar(100) DEFAULT NULL,
  `parentId` varchar(45) DEFAULT NULL,
  `createdBy` int(10) DEFAULT NULL,
  `createdDate` varchar(100) DEFAULT NULL,
  `modifiedDate` varchar(100) DEFAULT NULL,
  `modifiedBy` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 as default with Active and 0 as Inactive',
  `shortDescription` varchar(256) DEFAULT NULL,
  `longDescription` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `clientId` int(10) NOT NULL AUTO_INCREMENT,
  `clientName` varchar(50) DEFAULT NULL,
  `clientDesc` varchar(50) DEFAULT NULL,
  `flavourId` int(10) NOT NULL,
  PRIMARY KEY (`clientId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `communication_details`
--

DROP TABLE IF EXISTS `communication_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communication_details` (
  `uuid` varchar(100) NOT NULL DEFAULT '',
  `field1` longblob,
  `field2` longblob,
  `field3` longblob,
  PRIMARY KEY (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `id` varchar(20) DEFAULT NULL,
  `addedtime` varchar(20) DEFAULT NULL,
  `updatetime` varchar(20) DEFAULT NULL,
  `json` longblob,
  `name` varchar(200) DEFAULT NULL,
  `userid` varchar(20) DEFAULT NULL,
  `class` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `typeOfActiveInstances` varchar(50) DEFAULT NULL,
  `noOfActiveInstances` varchar(50) DEFAULT NULL,
  `ownerProjectId` varchar(100) DEFAULT NULL,
  KEY `idx_id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuration_version`
--

DROP TABLE IF EXISTS `configuration_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration_version` (
  `dataSourceVersionId` varchar(45) DEFAULT NULL,
  `fieldMappingVersionId` varchar(45) DEFAULT NULL,
  `dataSourceId` varchar(45) DEFAULT NULL,
  `dataSource` longblob,
  `fieldMappings` longblob,
  `insertedBy` varchar(45) DEFAULT NULL,
  `insertedOn` varchar(45) DEFAULT NULL,
  `dataAtRestFileType` varchar(45) DEFAULT NULL,
  `dataAtRestCompressionType` varchar(45) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customQueryFieldMapping`
--

DROP TABLE IF EXISTS `customQueryFieldMapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customQueryFieldMapping` (
  `nodeId` varchar(40) DEFAULT NULL,
  `appId` varchar(40) DEFAULT NULL,
  `data` longblob,
  `errorMessage` blob,
  `query` blob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customcomponentinstance`
--

DROP TABLE IF EXISTS `customcomponentinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customcomponentinstance` (
  `componentId` varchar(40) DEFAULT NULL,
  `componentName` varchar(40) DEFAULT NULL,
  `componentType` varchar(40) DEFAULT NULL,
  `componentDescription` varchar(400) DEFAULT NULL,
  `owner` varchar(40) DEFAULT NULL,
  `componentCreatedDate` varchar(40) DEFAULT NULL,
  `componentUpdatedDate` varchar(40) DEFAULT NULL,
  `componentStatus` varchar(40) DEFAULT NULL,
  `customYamlConfigData` blob,
  `supportingComponentType` varchar(40) DEFAULT NULL,
  `installLocation` varchar(200) DEFAULT NULL,
  `supportingComponentJarsPath` varchar(400) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `distFilePath` varchar(200) DEFAULT NULL,
  `yamlFilePath` varchar(200) DEFAULT NULL,
  `ownerProjectId` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `custquery`
--

DROP TABLE IF EXISTS `custquery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custquery` (
  `mayuri` varchar(20) DEFAULT NULL,
  `jamir` varchar(30) DEFAULT NULL,
  `alim` varchar(40) DEFAULT NULL,
  `balkrushna` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dataParamInstance`
--

DROP TABLE IF EXISTS `dataParamInstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataParamInstance` (
  `id` int(11) DEFAULT NULL,
  `dsId` varchar(100) DEFAULT NULL,
  `drId` varchar(100) DEFAULT NULL,
  `baapJobInsId` varchar(100) DEFAULT NULL,
  `paramName` varchar(100) DEFAULT NULL,
  `val` longblob,
  `type` varchar(200) DEFAULT NULL,
  `appId` varchar(200) DEFAULT NULL,
  `inputParamType` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `data_request`
--

DROP TABLE IF EXISTS `data_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) DEFAULT NULL,
  `requestedBy` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `last_modified_by` int(11) DEFAULT '0',
  `justification` text,
  `created_date` varchar(50) DEFAULT '0',
  `modified_date` varchar(50) DEFAULT '0',
  `group_id` int(11) DEFAULT '0',
  `rejected_justification` text,
  `requested_user_id` int(11) DEFAULT NULL,
  `objectType` varchar(50) DEFAULT NULL,
  `requestedAccess` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `database_function_types`
--

DROP TABLE IF EXISTS `database_function_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `database_function_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `database_function_udfs`
--

DROP TABLE IF EXISTS `database_function_udfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `database_function_udfs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UdfType` varchar(100) NOT NULL,
  `className` varchar(100) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `database_functions`
--

DROP TABLE IF EXISTS `database_functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `database_functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `parameters` varchar(100) NOT NULL,
  `returnType` varchar(100) NOT NULL,
  `databaseFunctionTypeId` int(11) NOT NULL,
  `databaseNameId` int(11) NOT NULL,
  `databaseFunctionUDFId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `database_names`
--

DROP TABLE IF EXISTS `database_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `database_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dataframe_export`
--

DROP TABLE IF EXISTS `dataframe_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataframe_export` (
  `PersonID` int(11) DEFAULT NULL,
  `LastName` text,
  `FirstName` text,
  `Address` text,
  `City` text,
  `field2` text,
  `cityStatus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datarepoinstance`
--

DROP TABLE IF EXISTS `datarepoinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datarepoinstance` (
  `dataRepoStatus` varchar(40) DEFAULT NULL,
  `repoUpdated` varchar(40) DEFAULT NULL,
  `repoCreated` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `totalRecords` varchar(40) DEFAULT NULL,
  `dataSourceIds` longblob,
  `filePaths` varchar(400) DEFAULT NULL,
  `refreshType` varchar(40) DEFAULT NULL,
  `size` varchar(40) DEFAULT NULL,
  `id` varchar(40) DEFAULT NULL,
  `repoName` varchar(40) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `userId` varchar(40) DEFAULT NULL,
  `lastUpdateFile` varchar(40) DEFAULT NULL,
  `repoId` varchar(40) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `ownerProjectId` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datasource_approval_mapper`
--

DROP TABLE IF EXISTS `datasource_approval_mapper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasource_approval_mapper` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `objectId` int(10) NOT NULL,
  `level` int(10) NOT NULL,
  `parent_level` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datasource_relations`
--

DROP TABLE IF EXISTS `datasource_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasource_relations` (
  `unqDsRelationsId` int(10) NOT NULL AUTO_INCREMENT,
  `dataSourceId` varchar(40) DEFAULT NULL,
  `relatedDsId` varchar(10) DEFAULT NULL,
  `relatedColumnsPair` varchar(800) DEFAULT NULL,
  `insertedBy` varchar(40) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` varchar(40) DEFAULT NULL,
  `updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`unqDsRelationsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datasource_run_status`
--

DROP TABLE IF EXISTS `datasource_run_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasource_run_status` (
  `workflowId` varchar(40) DEFAULT NULL,
  `appId` varchar(40) DEFAULT NULL,
  `nodeId` varchar(40) DEFAULT NULL,
  `loadStatus` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datasource_service_apimanager`
--

DROP TABLE IF EXISTS `datasource_service_apimanager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasource_service_apimanager` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `dataSourceId` blob,
  `dataSourceName` blob,
  `userId` int(10) DEFAULT NULL,
  `apiKey` varchar(50) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `deleted` int(10) DEFAULT NULL,
  `createdDate` varchar(40) DEFAULT NULL,
  `modifiedDate` varchar(40) DEFAULT NULL,
  `query` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `datasource_view`
--

DROP TABLE IF EXISTS `datasource_view`;
/*!50001 DROP VIEW IF EXISTS `datasource_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `datasource_view` AS SELECT 
 1 AS `groupId`,
 1 AS `createdBy`,
 1 AS `dataSourceName`,
 1 AS `dataRepoName`,
 1 AS `dataSourceType`,
 1 AS `updatedDate`,
 1 AS `category`,
 1 AS `subCategory`,
 1 AS `totalRecords`,
 1 AS `ownerProjectId`,
 1 AS `datasourceid`,
 1 AS `avgRating`,
 1 AS `accessType`,
 1 AS `fileType`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `datasourceinstance`
--

DROP TABLE IF EXISTS `datasourceinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasourceinstance` (
  `id` varchar(40) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `updatedDate` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `totalRecords` varchar(40) DEFAULT NULL,
  `latestFilepath` mediumtext,
  `dataSourceStatus` varchar(40) DEFAULT NULL,
  `filePaths` varchar(400) DEFAULT NULL,
  `dataSourceName` varchar(100) DEFAULT NULL,
  `createdDate` varchar(40) DEFAULT NULL,
  `dataSourceId` varchar(40) DEFAULT NULL,
  `size` varchar(40) DEFAULT NULL,
  `appId` varchar(50) DEFAULT NULL,
  `dataRepoId` varchar(40) DEFAULT NULL,
  `dataSourceType` varchar(40) DEFAULT NULL,
  `lastRunBy` varchar(40) DEFAULT NULL,
  `dataRepoName` varchar(40) DEFAULT NULL,
  `ingection` varchar(100) DEFAULT NULL,
  `oldFilePath` varchar(500) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `fileType` varchar(20) DEFAULT NULL,
  `compressionType` varchar(20) DEFAULT NULL,
  `isModified` int(1) DEFAULT '1',
  `fileTypeIndicator` varchar(50) DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `subCategory` varchar(100) DEFAULT NULL,
  `description` text,
  `avgRating` varchar(45) DEFAULT NULL,
  `flowType` varchar(45) DEFAULT NULL,
  `dataSourceVersionId` varchar(45) DEFAULT NULL,
  `fieldMappingVersionId` varchar(45) DEFAULT NULL,
  `ownerProjectId` varchar(100) DEFAULT NULL,
  KEY `idx_createdBy` (`createdBy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datasourceinstancehistory`
--

DROP TABLE IF EXISTS `datasourceinstancehistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasourceinstancehistory` (
  `dataSourceId` varchar(40) DEFAULT NULL,
  `dataSourceName` varchar(255) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `createdDate` varchar(40) DEFAULT NULL,
  `filePath` varchar(500) DEFAULT NULL,
  `totalRecords` varchar(40) DEFAULT NULL,
  `size` varchar(40) DEFAULT NULL,
  `fileType` varchar(20) DEFAULT NULL,
  `compressionType` varchar(20) DEFAULT NULL,
  `dataSourceStatus` varchar(20) DEFAULT NULL,
  `dataSourceType` varchar(40) DEFAULT NULL,
  `updatedDate` varchar(40) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobInstId` varchar(40) NOT NULL,
  `updatedBy` varchar(40) DEFAULT NULL,
  `dataSourceVersionId` varchar(50) DEFAULT NULL,
  `fieldMappingVersionId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datasourcesnapshot_detail`
--

DROP TABLE IF EXISTS `datasourcesnapshot_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasourcesnapshot_detail` (
  `snapshotId` int(11) DEFAULT NULL,
  `jobInstId` varchar(40) DEFAULT NULL,
  `status` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `datasourcesnapshot_master`
--

DROP TABLE IF EXISTS `datasourcesnapshot_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasourcesnapshot_master` (
  `snapshotId` int(11) NOT NULL AUTO_INCREMENT,
  `dataSourceId` varchar(40) DEFAULT NULL,
  `snapshotDate` varchar(40) DEFAULT NULL,
  `snapshotStatus` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`snapshotId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `demog_details_rdbms`
--

DROP TABLE IF EXISTS `demog_details_rdbms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demog_details_rdbms` (
  `cust_id` varchar(50) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `emailid` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `pin` varchar(50) DEFAULT NULL,
  `locality` varchar(50) DEFAULT NULL,
  `age` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `errored_records`
--

DROP TABLE IF EXISTS `errored_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `errored_records` (
  `id` varchar(40) DEFAULT NULL,
  `jobId` varchar(40) DEFAULT NULL,
  `errorType` varchar(40) DEFAULT NULL,
  `erroredRecord` blob,
  `class` varchar(40) DEFAULT NULL,
  `errorField` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `execute_queries`
--

DROP TABLE IF EXISTS `execute_queries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `execute_queries` (
  `id` varchar(500) DEFAULT NULL,
  `input` longblob,
  `output` longblob,
  `error` longblob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `externaldatadetails`
--

DROP TABLE IF EXISTS `externaldatadetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `externaldatadetails` (
  `id` int(50) DEFAULT NULL,
  `dataSourceId` int(50) DEFAULT NULL,
  `amazonS3` blob,
  `googleAnalytics` blob,
  `delimeter` varchar(20) DEFAULT NULL,
  `storagePath` varchar(500) DEFAULT NULL,
  `salesforce` blob,
  `dropbox` blob,
  `facebookPage` blob,
  `connectionName` varchar(500) DEFAULT NULL,
  `hackyl` blob,
  `Probe42` blob,
  `SapErp` blob,
  `connectortype` varchar(500) DEFAULT NULL,
  `containsHeader` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` varchar(40) DEFAULT NULL,
  `dataSourceId` varchar(40) DEFAULT NULL,
  `unqUserId` int(20) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `rating` varchar(20) DEFAULT NULL,
  `question` varchar(50) DEFAULT NULL,
  `answer` blob,
  `insertTS` varchar(100) DEFAULT NULL,
  `userName` varchar(100) DEFAULT NULL,
  KEY `idx_rating` (`rating`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `feedback_view`
--

DROP TABLE IF EXISTS `feedback_view`;
/*!50001 DROP VIEW IF EXISTS `feedback_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `feedback_view` AS SELECT 
 1 AS `dataSourceId`,
 1 AS `avgRating`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `feedback_voting`
--

DROP TABLE IF EXISTS `feedback_voting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_voting` (
  `id` int(11) NOT NULL,
  `feedbackId` int(11) NOT NULL,
  `dataSourceId` int(11) NOT NULL,
  `unqUserId` int(11) NOT NULL,
  `created_date` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL COMMENT 'Keep it in case if want to vote agains datasource/flow',
  `vote` int(11) NOT NULL COMMENT 'use ''1'' - voted as positve & ''-1'' - voted as negative',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flavour`
--

DROP TABLE IF EXISTS `flavour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flavour` (
  `flavourId` int(10) NOT NULL AUTO_INCREMENT,
  `flavourName` varchar(50) DEFAULT NULL,
  `flavourDesc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`flavourId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flavour_action_mappings`
--

DROP TABLE IF EXISTS `flavour_action_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flavour_action_mappings` (
  `flavourActionId` int(10) NOT NULL AUTO_INCREMENT,
  `flavourId` int(10) NOT NULL,
  `actionsMasterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`flavourActionId`)
) ENGINE=InnoDB AUTO_INCREMENT=511 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flavour_persona_mappings`
--

DROP TABLE IF EXISTS `flavour_persona_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flavour_persona_mappings` (
  `flavourPersonaId` int(10) NOT NULL AUTO_INCREMENT,
  `flavourId` int(10) NOT NULL,
  `personaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`flavourPersonaId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `glossary_master`
--

DROP TABLE IF EXISTS `glossary_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `glossary_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `shortDescription` varchar(100) DEFAULT NULL,
  `longDescription` text,
  `createdDate` varchar(45) NOT NULL,
  `createdBy` varchar(45) NOT NULL,
  `modifiedBy` varchar(45) DEFAULT NULL,
  `modifiedDate` varchar(45) DEFAULT NULL,
  `groupId` int(10) NOT NULL,
  `status` int(1) DEFAULT '1' COMMENT 'Default 1 is Active & 0 is Inactive',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_object_mapping`
--

DROP TABLE IF EXISTS `group_object_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_object_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `objectId` int(11) NOT NULL,
  `objectType` varchar(100) NOT NULL,
  `accessType` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_project_mapping`
--

DROP TABLE IF EXISTS `group_project_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_project_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `projectId` int(11) NOT NULL,
  `accessType` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hiveQuery`
--

DROP TABLE IF EXISTS `hiveQuery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hiveQuery` (
  `id` varchar(40) DEFAULT NULL,
  `queryName` varchar(40) DEFAULT NULL,
  `query` blob,
  `userId` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `delimiter` varchar(20) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `hbase` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `index_defination`
--

DROP TABLE IF EXISTS `index_defination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `index_defination` (
  `id` decimal(10,0) DEFAULT NULL,
  `indexName` varchar(500) DEFAULT NULL,
  `indexFields` blob,
  `createdOn` varchar(40) DEFAULT NULL,
  `lastRunOn` varchar(40) DEFAULT NULL,
  `runningStatus` varchar(100) DEFAULT NULL,
  `createdBy` varchar(100) DEFAULT NULL,
  `count` decimal(10,0) DEFAULT NULL,
  `dsId` decimal(10,0) DEFAULT NULL,
  `appId` decimal(10,0) DEFAULT NULL,
  `nodeId` mediumtext,
  `searchInputType` varchar(100) DEFAULT NULL,
  `indexInputType` varchar(100) DEFAULT NULL,
  `sampleJson` blob,
  `inputDataDilimiter` varchar(40) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `ownerProjectId` int(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `job_instances`
--

DROP TABLE IF EXISTS `job_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_instances` (
  `topic` varchar(40) DEFAULT NULL,
  `startTime` varchar(40) DEFAULT NULL,
  `parentJobInstId` varchar(40) DEFAULT NULL,
  `messageParam` varchar(5000) DEFAULT NULL,
  `level` varchar(40) DEFAULT NULL,
  `typeId` varchar(40) DEFAULT NULL,
  `baapJobInstId` varchar(40) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `userId` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `endTime` varchar(40) DEFAULT NULL,
  `jobName` varchar(400) DEFAULT NULL,
  `jobInstId` varchar(40) DEFAULT NULL,
  `hadoopJobId` varchar(100) DEFAULT NULL,
  `jobStatus` varchar(100) DEFAULT NULL,
  `errorLog` blob,
  `jobTrackingUrl` varchar(400) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `insertDate` varchar(40) DEFAULT NULL,
  `hiveTableName` varchar(400) DEFAULT NULL,
  `offsetInfo` blob,
  `jobCounters` blob,
  `groupId` int(10) DEFAULT NULL,
  `nodeId` varchar(45) DEFAULT NULL,
  `hadoopJobName` varchar(200) DEFAULT NULL,
  `killedBy` varchar(50) DEFAULT NULL,
  `errorMsg` blob,
  `stageLevel` int(10) DEFAULT NULL,
  KEY `typeId_index` (`typeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jupyter`
--

DROP TABLE IF EXISTS `jupyter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jupyter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notebookName` varchar(40) DEFAULT NULL,
  `createdOn` varchar(40) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL,
  `ownerProjectId` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_track_workflowInstance`
--

DROP TABLE IF EXISTS `m_track_workflowInstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_track_workflowInstance` (
  `workflowId` varchar(40) DEFAULT NULL,
  `componentName` varchar(100) DEFAULT NULL,
  `statusEnum` varchar(40) DEFAULT NULL,
  `componentId` varchar(100) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `userId` varchar(40) DEFAULT NULL,
  `lastRun` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `createSt` varchar(40) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `appCombiner` blob,
  `groupId` int(10) DEFAULT NULL,
  `start_at` varchar(45) DEFAULT NULL,
  `run_by` varchar(45) DEFAULT NULL,
  `end_at` varchar(45) DEFAULT NULL,
  `ownerProjectId` int(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `machine_learning_models`
--

DROP TABLE IF EXISTS `machine_learning_models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `machine_learning_models` (
  `modelId` int(11) NOT NULL AUTO_INCREMENT,
  `modelName` varchar(255) DEFAULT NULL,
  `modelLocation` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `groupId` varchar(255) DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `createdDate` varchar(45) DEFAULT NULL,
  `modifiedDate` varchar(45) DEFAULT NULL,
  `modifiedBy` varchar(45) DEFAULT NULL,
  `modelType` varchar(45) DEFAULT NULL,
  `apiKey` text,
  `apiAccessStatus` int(1) DEFAULT NULL,
  `data` longblob,
  `ownerProjectId` int(45) DEFAULT NULL,
  `appId` mediumtext,
  `nodeId` mediumtext,
  `jobInstanceId` mediumtext,
  `modelPackageName` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`modelId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `masterListOfApplication`
--

DROP TABLE IF EXISTS `masterListOfApplication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `masterListOfApplication` (
  `unqAppId` int(11) NOT NULL AUTO_INCREMENT,
  `appId` int(11) NOT NULL,
  `appName` varchar(45) NOT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTs` timestamp NULL DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `updatedTs` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`unqAppId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maxiq_apps`
--

DROP TABLE IF EXISTS `maxiq_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maxiq_apps` (
  `appName` varchar(100) DEFAULT NULL,
  `statusEnum` varchar(40) DEFAULT NULL,
  `appId` varchar(40) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `userId` varchar(40) DEFAULT NULL,
  `lastRun` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `createSt` varchar(40) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `appCombiner` blob,
  `groupId` int(10) DEFAULT NULL,
  `isModified` int(1) DEFAULT '1',
  `scheduled` varchar(10) DEFAULT NULL,
  `flowType` varchar(45) DEFAULT NULL,
  `pollInterval` int(45) DEFAULT NULL,
  `ownerProjectId` int(45) DEFAULT NULL,
  `runningStatus` varchar(20) DEFAULT 'STOPPED'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maxiq_commons`
--

DROP TABLE IF EXISTS `maxiq_commons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maxiq_commons` (
  `id` varchar(500) DEFAULT NULL,
  `group_name` varchar(500) DEFAULT NULL,
  `keyOf` varchar(500) DEFAULT NULL,
  `value` varchar(500) DEFAULT NULL,
  `status` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maxiq_metadata`
--

DROP TABLE IF EXISTS `maxiq_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maxiq_metadata` (
  `id` varchar(50) DEFAULT NULL,
  `dataSourceId` varchar(50) DEFAULT NULL,
  `metaData` blob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_collection`
--

DROP TABLE IF EXISTS `my_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_collection` (
  `id` int(15) NOT NULL,
  `objectId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `created_date` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT 'By defaullt status is 1 as active record \nif 0 then which is removed OR Inactive',
  `objectType` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `my_solutions`
--

DROP TABLE IF EXISTS `my_solutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_solutions` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `installation_id` varchar(150) DEFAULT NULL,
  `solution_id` varchar(150) DEFAULT NULL,
  `solution_name` varchar(200) DEFAULT NULL,
  `solution_short_desc` varchar(300) DEFAULT NULL,
  `solution_full_desc` varchar(1000) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `created_date` varchar(45) DEFAULT NULL,
  `version_id` varchar(45) DEFAULT NULL,
  `version_name` varchar(45) DEFAULT NULL,
  `status` bit(1) DEFAULT b'1',
  `launch_url` varchar(350) DEFAULT NULL,
  `group_id` varchar(45) DEFAULT '0',
  `project_id` varchar(45) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `objectType` int(11) NOT NULL,
  `actionId` int(11) NOT NULL,
  `notificationType` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona_action_mapping`
--

DROP TABLE IF EXISTS `persona_action_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_action_mapping` (
  `persona_id` int(20) DEFAULT NULL,
  `actionsMasterId` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona_master`
--

DROP TABLE IF EXISTS `persona_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_master` (
  `persona_id` int(20) DEFAULT NULL,
  `persona_name` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pin_lookup`
--

DROP TABLE IF EXISTS `pin_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pin_lookup` (
  `c1` int(11) DEFAULT NULL,
  `c2` varchar(45) DEFAULT NULL,
  `c3` varchar(45) DEFAULT NULL,
  `c4` varchar(45) DEFAULT NULL,
  `c5` varchar(45) DEFAULT NULL,
  `c6` varchar(45) DEFAULT NULL,
  `c7` varchar(45) DEFAULT NULL,
  `c8` varchar(45) DEFAULT NULL,
  `c9` varchar(45) DEFAULT NULL,
  `c10` varchar(45) DEFAULT NULL,
  `c11` varchar(45) DEFAULT NULL,
  `c12` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profiling_output`
--

DROP TABLE IF EXISTS `profiling_output`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiling_output` (
  `sourceId` int(11) DEFAULT NULL,
  `type` varchar(30) NOT NULL,
  `instId` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `ProfType` varchar(30) DEFAULT NULL,
  `field1` varchar(50) DEFAULT NULL,
  `field2` varchar(50) DEFAULT NULL,
  `Measurement` varchar(1000) DEFAULT NULL,
  `Value` double(65,30) DEFAULT NULL,
  `methodType` varchar(30) DEFAULT NULL,
  `nodeId` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_object_mapping`
--

DROP TABLE IF EXISTS `project_object_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_object_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` int(11) NOT NULL,
  `objectId` int(11) NOT NULL,
  `objectType` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_projectId` (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(200) NOT NULL,
  `description` longblob,
  `type` varchar(50) NOT NULL,
  `createdDate` varchar(50) NOT NULL,
  `modifiedDate` varchar(50) DEFAULT NULL,
  `createdBy` varchar(20) NOT NULL,
  `modifiedBy` varchar(20) DEFAULT NULL,
  `imageIcon` blob,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `queue_master`
--

DROP TABLE IF EXISTS `queue_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_master` (
  `queueMasterId` int(11) NOT NULL AUTO_INCREMENT,
  `queueName` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`queueMasterId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roleActionHistory`
--

DROP TABLE IF EXISTS `roleActionHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roleActionHistory` (
  `roleActionHistoryId` int(10) NOT NULL AUTO_INCREMENT,
  `his_unqRoleMasterId` int(11) DEFAULT NULL,
  `his_roleName` varchar(30) DEFAULT NULL,
  `his_roleDescription` varchar(100) DEFAULT NULL,
  `his_createdBy` varchar(40) DEFAULT NULL,
  `his_createdTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `his_updatedBy` varchar(40) DEFAULT NULL,
  `his_updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `his_roleActionIdList` varchar(10000) DEFAULT NULL,
  `his_actionIdList` varchar(10000) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `insertBy` varchar(40) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`roleActionHistoryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_action_mappings`
--

DROP TABLE IF EXISTS `role_action_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_action_mappings` (
  `roleActionId` int(11) NOT NULL AUTO_INCREMENT,
  `roleMasterId` int(11) DEFAULT NULL,
  `actionId` int(11) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `insertedBy` varchar(40) DEFAULT NULL,
  `updatedBy` varchar(40) DEFAULT NULL,
  `updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`roleActionId`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_master`
--

DROP TABLE IF EXISTS `role_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_master` (
  `unqRoleMasterId` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(30) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `roleDescription` varchar(400) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `createdTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` varchar(40) DEFAULT NULL,
  `updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `appId` int(11) DEFAULT NULL,
  PRIMARY KEY (`unqRoleMasterId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roleactionhistory`
--

DROP TABLE IF EXISTS `roleactionhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roleactionhistory` (
  `roleActionHistoryId` int(10) NOT NULL AUTO_INCREMENT,
  `his_unqRoleMasterId` int(11) DEFAULT NULL,
  `his_roleName` varchar(30) DEFAULT NULL,
  `his_roleDescription` varchar(100) DEFAULT NULL,
  `his_createdBy` varchar(40) DEFAULT NULL,
  `his_createdTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `his_updatedBy` varchar(40) DEFAULT NULL,
  `his_updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `his_roleActionIdList` blob,
  `his_actionIdList` blob,
  `action` blob,
  `insertBy` varchar(40) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`roleActionHistoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scheduled_application_details`
--

DROP TABLE IF EXISTS `scheduled_application_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheduled_application_details` (
  `id` int(100) DEFAULT NULL,
  `appId` int(100) DEFAULT NULL,
  `createdBy` int(100) DEFAULT NULL,
  `cronExpression` varchar(1000) DEFAULT NULL,
  `oozieJobId` varchar(100) DEFAULT NULL,
  `groupId` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_query_def`
--

DROP TABLE IF EXISTS `search_query_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_query_def` (
  `id` decimal(10,0) DEFAULT NULL,
  `queryName` varchar(500) DEFAULT NULL,
  `indexName` varchar(500) DEFAULT NULL,
  `queries` blob,
  `aggregations` blob,
  `createdOn` varchar(40) DEFAULT NULL,
  `apiKey` varchar(100) DEFAULT NULL,
  `createBy` varchar(100) DEFAULT NULL,
  `outputFields` blob,
  `groupId` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sequence`
--

DROP TABLE IF EXISTS `sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence` (
  `counter` int(11) DEFAULT NULL,
  `tabename` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `share_access_history`
--

DROP TABLE IF EXISTS `share_access_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `share_access_history` (
  `shareaccHist_Id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `assetType` varchar(50) NOT NULL,
  `groupId` varchar(20) DEFAULT NULL,
  `userId` varchar(20) DEFAULT NULL,
  `access` varchar(50) NOT NULL,
  `accessType` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`shareaccHist_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `socialappinstance`
--

DROP TABLE IF EXISTS `socialappinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialappinstance` (
  `id` varchar(40) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `type` varchar(40) DEFAULT NULL,
  `appId` varchar(40) DEFAULT NULL,
  `secretKey` varchar(40) DEFAULT NULL,
  `owner` varchar(40) DEFAULT NULL,
  `createdDate` varchar(40) DEFAULT NULL,
  `updatedDate` varchar(40) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `solution_objects`
--

DROP TABLE IF EXISTS `solution_objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solution_objects` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `solution_id` varchar(100) DEFAULT NULL,
  `mp_id` int(50) DEFAULT NULL,
  `object_type` varchar(20) DEFAULT NULL,
  `object_id` varchar(40) DEFAULT NULL,
  `installedBy` varchar(50) NOT NULL DEFAULT '',
  `installedDate` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `timeSeriesComponents`
--

DROP TABLE IF EXISTS `timeSeriesComponents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeSeriesComponents` (
  `id` varchar(40) DEFAULT NULL,
  `componentName` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `componentType` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `timeSeriesData`
--

DROP TABLE IF EXISTS `timeSeriesData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeSeriesData` (
  `timeStamp` varchar(40) DEFAULT NULL,
  `value` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `hbaseRowKey` varchar(40) DEFAULT NULL,
  `hostName` varchar(50) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userApplicationMapping`
--

DROP TABLE IF EXISTS `userApplicationMapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userApplicationMapping` (
  `unqUserAppMap` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `appId` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTs` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTs` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`unqUserAppMap`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_client_mapping`
--

DROP TABLE IF EXISTS `user_client_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_client_mapping` (
  `unqUserId` int(20) NOT NULL,
  `clientId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_feedback`
--

DROP TABLE IF EXISTS `user_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_feedback` (
  `userName` varchar(40) DEFAULT NULL,
  `businessEmail` varchar(40) DEFAULT NULL,
  `comments` varchar(50) DEFAULT NULL,
  `id` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_group_mappings`
--

DROP TABLE IF EXISTS `user_group_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group_mappings` (
  `unqUserGroupMapId` int(20) NOT NULL AUTO_INCREMENT,
  `userId` int(20) DEFAULT NULL,
  `groupId` int(20) DEFAULT NULL,
  `inserteBy` varchar(40) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` varchar(40) DEFAULT NULL,
  `updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`unqUserGroupMapId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_object_mapping`
--

DROP TABLE IF EXISTS `user_object_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_object_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `objectId` int(11) NOT NULL,
  `objectType` varchar(100) NOT NULL,
  `accessType` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_persona`
--

DROP TABLE IF EXISTS `user_persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_persona` (
  `unqUserId` int(20) DEFAULT NULL,
  `persona_id` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_project_mapping`
--

DROP TABLE IF EXISTS `user_project_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_project_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `projectId` int(11) NOT NULL,
  `accessType` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_role_mappings`
--

DROP TABLE IF EXISTS `user_role_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role_mappings` (
  `unqUserRoleMapId` int(10) NOT NULL AUTO_INCREMENT,
  `userId` varchar(40) DEFAULT NULL,
  `roleId` int(10) DEFAULT NULL,
  `insertedBy` varchar(40) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` varchar(40) DEFAULT NULL,
  `updatedTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`unqUserRoleMapId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_subgroup`
--

DROP TABLE IF EXISTS `user_subgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_subgroup` (
  `subGroupId` int(10) NOT NULL AUTO_INCREMENT,
  `subGroupName` varchar(400) DEFAULT NULL,
  `subGroupDescription` varchar(1000) DEFAULT NULL,
  `usergroupId` int(11) DEFAULT NULL,
  `insertedBy` varchar(100) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` varchar(100) DEFAULT NULL,
  `updateTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `queueMasterID` int(11) DEFAULT NULL,
  PRIMARY KEY (`subGroupId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_subgroup_history`
--

DROP TABLE IF EXISTS `user_subgroup_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_subgroup_history` (
  `userSubgroup_history_id` int(10) NOT NULL AUTO_INCREMENT,
  `his_subGroupId` int(10) DEFAULT NULL,
  `his_subGroupName` varchar(400) DEFAULT NULL,
  `his_subGroupDescription` varchar(1000) DEFAULT NULL,
  `his_usergroupId` int(11) DEFAULT NULL,
  `his_groupName` varchar(400) DEFAULT NULL,
  `his_insertedBy` varchar(100) DEFAULT NULL,
  `his_insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `his_updatedBy` varchar(100) DEFAULT NULL,
  `his_updateTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action` varchar(20) DEFAULT NULL,
  `insertedBy` varchar(100) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`userSubgroup_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_subgroup_mappings`
--

DROP TABLE IF EXISTS `user_subgroup_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_subgroup_mappings` (
  `unqUserSubgrpMapId` int(10) NOT NULL AUTO_INCREMENT,
  `userId` varchar(40) DEFAULT NULL,
  `subgroupId` int(20) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insertedBy` varchar(40) DEFAULT NULL,
  `updateTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedBy` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`unqUserSubgrpMapId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usergroup`
--

DROP TABLE IF EXISTS `usergroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroup` (
  `groupId` int(10) NOT NULL AUTO_INCREMENT,
  `groupName` varchar(400) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `queueMasterID` int(11) DEFAULT NULL,
  `insertedBy` varchar(100) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` varchar(100) DEFAULT NULL,
  `updateTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `appId` int(11) DEFAULT NULL,
  `rangerGroupId` int(11) DEFAULT NULL,
  `rangerGroupUserId` int(11) DEFAULT NULL,
  `rangerGroupName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`groupId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usergroup_history`
--

DROP TABLE IF EXISTS `usergroup_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroup_history` (
  `usergroup_history_id` int(10) NOT NULL AUTO_INCREMENT,
  `his_groupId` int(10) DEFAULT NULL,
  `his_groupName` varchar(400) DEFAULT NULL,
  `his_description` varchar(1000) DEFAULT NULL,
  `his_queueMasterID` int(11) DEFAULT NULL,
  `his_queueName` varchar(100) DEFAULT NULL,
  `his_insertedBy` varchar(100) DEFAULT NULL,
  `his_insertTs` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `his_updatedBy` varchar(100) DEFAULT NULL,
  `his_updateTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action` varchar(20) DEFAULT NULL,
  `insertedBy` varchar(100) DEFAULT NULL,
  `insertTs` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`usergroup_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workflow_graph`
--

DROP TABLE IF EXISTS `workflow_graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflow_graph` (
  `id` varchar(40) DEFAULT NULL,
  `class` varchar(40) DEFAULT NULL,
  `linkInfo` blob,
  `canvas_graphData` blob,
  `nodeInfo` blob,
  `createdBy` varchar(40) DEFAULT NULL,
  `createdTime` varchar(40) DEFAULT NULL,
  `updatedTime` varchar(40) DEFAULT NULL,
  `updatedBy` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `workflow_process_info`
--

DROP TABLE IF EXISTS `workflow_process_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflow_process_info` (
  `id` varchar(40) DEFAULT NULL,
  `nodeId` varchar(40) DEFAULT NULL,
  `appId` varchar(40) DEFAULT NULL,
  `data` longblob,
  `class` varchar(40) DEFAULT NULL,
  `indicator` varchar(100) DEFAULT NULL,
  `nodeStatus` varchar(20) DEFAULT NULL,
  KEY `nodeId` (`nodeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zeppelin_configure`
--

DROP TABLE IF EXISTS `zeppelin_configure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zeppelin_configure` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `zeppelinId` varchar(100) DEFAULT NULL,
  `notebookName` varchar(100) DEFAULT NULL,
  `createdOn` varchar(40) DEFAULT NULL,
  `createdBy` varchar(40) DEFAULT NULL,
  `groupId` int(40) DEFAULT NULL,
  `projectId` int(45) DEFAULT NULL,
  `ownerProjectId` int(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `datasource_view`
--

/*!50001 DROP VIEW IF EXISTS `datasource_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `datasource_view` AS select `dto`.`groupId` AS `groupId`,concat(`app`.`firstName`,' ',`app`.`lastName`) AS `createdBy`,`dto`.`dataSourceName` AS `dataSourceName`,`dto`.`dataRepoName` AS `dataRepoName`,`dto`.`dataSourceType` AS `dataSourceType`,`dto`.`updatedDate` AS `updatedDate`,`dto`.`category` AS `category`,`dto`.`subCategory` AS `subCategory`,`dto`.`totalRecords` AS `totalRecords`,`dto`.`ownerProjectId` AS `ownerProjectId`,`dto`.`dataSourceId` AS `datasourceid`,`fbk`.`avgRating` AS `avgRating`,'LOAD_EDIT' AS `accessType`,`dto`.`fileType` AS `fileType` from ((`datasourceinstance` `dto` left join `application_user` `app` on((`app`.`unqUserId` = `dto`.`createdBy`))) left join `feedback_view` `fbk` on((`dto`.`dataSourceId` = `fbk`.`dataSourceId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `feedback_view`
--

/*!50001 DROP VIEW IF EXISTS `feedback_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `feedback_view` AS select `feedback`.`dataSourceId` AS `dataSourceId`,ifnull(round(ifnull(avg(`feedback`.`rating`),0),0),0) AS `avgRating` from `feedback` where ((`feedback`.`rating` <> 0) and (`feedback`.`rating` is not null) and (`feedback`.`rating` <> 'null')) group by `feedback`.`dataSourceId` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-01  2:25:05
