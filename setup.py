from setuptools import setup, find_packages


setup(
    name='mosaicdb',
    version='1.0.0',
    description='db schema and initial data for mosaic decisions',
    url='https://bitbucket.org/maxiqdev/mosaicdb',
    author='Akhil Lawrence',
    author_email='akhil.lawrence@lntinfotech.com',
    license='MIT',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['click'],
    entry_points={
        'console_scripts': [
            'install-mosaicdb=mosaicdb.commands:install',
            'install-mosaicdb-schema=mosaicdb.commands:install_schema',
            'install-mosaicdb-data=mosaicdb.commands:install_data',
        ],
    },
)

